/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strpbrk.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:57 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:58 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strpbrk(const char *str, const char *charset)
{
	const char	*cur;
	char		*ptr;

	while (*str)
	{
		cur = charset;
		while (*cur)
		{
			if ((ptr = ft_strchr(str, *cur)) != NULL)
				return ((char*)ptr);
			cur++;
		}
		str++;
	}
	return (NULL);
}
