/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strctrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:26 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:26 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strctrim(const char *s, char c)
{
	size_t	len;

	while (*s && *s == c)
		s++;
	len = ft_strlen(s);
	if (len == 0)
		return (ft_strdup(""));
	len--;
	while (*(s + len) == c)
		len--;
	return (ft_strsub(s, 0, len + 1));
}
