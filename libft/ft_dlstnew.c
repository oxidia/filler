/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:53 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:54 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlstnew(void const *content, size_t content_size)
{
	t_dlist	*elm;

	elm = (t_dlist*)ft_memalloc(sizeof(t_dlist));
	if (elm != NULL)
	{
		elm->content_size = content_size;
		elm->content = (void*)content;
		if (content == NULL)
			elm->content_size = 0;
	}
	return (elm);
}
