/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htnew.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:01 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:01 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_hashtable	*ft_htnew(int size)
{
	t_hashtable	*ht;

	ht = (t_hashtable*)ft_memalloc(sizeof(t_hashtable));
	if (ht != NULL)
	{
		ht->size = size;
		ht->table = (t_hentry**)ft_memalloc(sizeof(t_hentry*) * size);
		if (ht->table == NULL)
			ft_memdel((void**)&ht);
	}
	return (ht);
}
