/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:18:14 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:18:17 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	ft_call(char c)
{
	return ((char)ft_toupper((char)c));
}

char		*ft_strtoupper(const char *str)
{
	return (ft_strmap(str, &ft_call));
}
