/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtolower.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:18:12 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:18:12 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	ft_call(char c)
{
	return ((char)ft_tolower((char)c));
}

char		*ft_strtolower(const char *str)
{
	return (ft_strmap(str, &ft_call));
}
