/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:17 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:17 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
	t_list	*current;
	t_list	*to_del;

	current = *alst;
	while (current != NULL)
	{
		to_del = current;
		(*del)(to_del->content, to_del->content_size);
		current = current->next;
		free(to_del);
	}
	*alst = NULL;
}
