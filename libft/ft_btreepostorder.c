/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreepostorder.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:11 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:18 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_btreepostorder(t_btree *root, void (*ft)(t_btree*))
{
	if (root == NULL)
		return ;
	ft_btreepostorder(root->left, ft);
	ft_btreepostorder(root->right, ft);
	(*ft)(root);
}
