/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_indexof.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:02 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:02 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t	ft_indexof(const char *str, char c)
{
	ssize_t	i;

	i = 0;
	while (*(str + i))
	{
		if (*(str + i) == c)
			return (i);
		i++;
	}
	return (-1);
}
