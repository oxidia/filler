/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:31 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:31 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*elm;

	elm = (t_list*)ft_memalloc(sizeof(t_list));
	if (elm != NULL)
	{
		elm->content_size = content_size;
		elm->content = (void*)content;
		if (content == NULL)
			elm->content_size = 0;
		elm->next = NULL;
	}
	return (elm);
}
