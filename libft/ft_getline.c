/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:55 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/10/01 11:07:14 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_join(char *str, const char *buf)
{
	char	*res;

	res = ft_strjoin(str, buf);
	ft_strdel(&str);
	return (res);
}

int			ft_getline(const int fd, char **line)
{
	char	*str;
	char	buf[2];
	ssize_t	size;

	if (read(fd, buf, 0) < 0)
		return (-1);
	ft_bzero(buf, 2);
	if ((str = ft_strnew(0)) == NULL)
		return (-1);
	while ((size = read(fd, buf, 1)) > 0)
	{
		if (*buf == '\n')
			break ;
		if ((str = ft_join(str, buf)) == NULL)
			return (-1);
	}
	if (!*buf)
	{
		free(str);
		return (0);
	}
	*line = str;
	return (1);
}
