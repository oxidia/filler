/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vprintf_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:18:22 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:18:57 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_printbase_fd(unsigned long num, int base, int fd)
{
	static char	*str_base = "0123456789abcdef";

	if (num >= (unsigned long)base)
		ft_printbase_fd(num / (unsigned long)base, base, fd);
	ft_putchar_fd(*(str_base + num % (unsigned long)base), fd);
}

static void	ft_call(int fd, char conv, va_list arg)
{
	char	*str;

	if (conv == 's')
	{
		str = va_arg(arg, char*);
		ft_putstr_fd(str == NULL ? "(null)" : str, fd);
	}
	else if (conv == 'd')
		ft_putnbr_fd(va_arg(arg, int), fd);
	else if (conv == 'D')
		ft_putlnbr_fd(va_arg(arg, long), fd);
	else if (conv == 'u')
		ft_printbase_fd(va_arg(arg, unsigned int), 10, fd);
	else if (conv == 'U')
		ft_printbase_fd(va_arg(arg, unsigned long), 10, fd);
	else if (conv == 'c')
		ft_putchar_fd((char)va_arg(arg, int), fd);
	else if (conv == 'x')
		ft_printbase_fd(va_arg(arg, unsigned long), 16, fd);
	else if (conv == 'o')
		ft_printbase_fd(va_arg(arg, unsigned long), 8, fd);
	else if (conv == 'b')
		ft_printbase_fd(va_arg(arg, unsigned int), 2, fd);
	else if (conv == 'B')
		ft_printbase_fd(va_arg(arg, unsigned long), 2, fd);
}

void		ft_vprintf_fd(int fd, const char *format, va_list args)
{
	while (*format)
	{
		if (*format == '%')
		{
			ft_call(fd, *(format + 1), args);
			format += 2;
		}
		else
			ft_putchar_fd(*format++, fd);
	}
}
