/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hentrynew.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:57 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:58 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_hentry	*ft_hentrynew(const char *key, const char *value)
{
	t_hentry	*item;

	item = (t_hentry*)ft_memalloc(sizeof(t_hentry));
	if (item != NULL)
	{
		if ((item->key = ft_strdup(key)) == NULL)
			ft_memdel((void**)&item);
		else if ((item->value = ft_strdup(value)) == NULL)
		{
			ft_strdel(&item->key);
			ft_memdel((void**)&item);
		}
	}
	return (item);
}
