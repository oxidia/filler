/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hentrydel.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:56 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:57 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_hentrydel(t_hentry **item)
{
	if (item == NULL || *item == NULL)
		return ;
	ft_strdel(&(*item)->key);
	ft_strdel(&(*item)->value);
	ft_memdel((void**)item);
}
