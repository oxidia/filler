/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnumber.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/01 11:56:46 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/10/01 13:06:11 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isnumber(const char *str, char c)
{
	size_t	len;

	len = 0;
	while (1)
	{
		if (*str == c)
			break ;
		if (!ft_isdigit(*str))
			return (0);
		len++;
		str++;
	}
	return (len);
}
