/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:59 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:18:00 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t	len;

	len = 0;
	while (*(s + len))
		len++;
	while (len > 0)
	{
		if (*(s + len) == (char)c)
			return ((char*)(s + len));
		len--;
	}
	if (*(s + len) == (char)c)
		return ((char*)(s + len));
	return (NULL);
}
