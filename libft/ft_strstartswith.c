/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstartswith.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:18:06 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:18:07 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strstartswith(const char *str, const char *sub)
{
	size_t	sub_len;

	sub_len = ft_strlen(sub);
	if (sub_len == 0)
		return (1);
	return (ft_strncmp(str, sub, sub_len) == 0);
}
