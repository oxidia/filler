/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:55 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:55 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnjoin(unsigned int n, ...)
{
	char	*str;
	char	*to_del;
	va_list	args;

	str = ft_strnew(0);
	if (str != NULL)
	{
		va_start(args, n);
		while (n > 0)
		{
			to_del = str;
			str = ft_strjoin(str, va_arg(args, char*));
			ft_strdel(&to_del);
			if (str == NULL)
				break ;
			n--;
		}
		va_end(args);
	}
	return (str);
}
