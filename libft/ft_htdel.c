/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htdel.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:58 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:59 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_htdel(t_hashtable **ht)
{
	int			i;
	t_hentry	*cur;
	t_hentry	*to_del;

	if (ht == NULL)
		return ;
	if (*ht != NULL)
	{
		i = 0;
		while (i < (*ht)->size)
		{
			cur = (*ht)->table[i];
			while (cur != NULL)
			{
				to_del = cur;
				cur = cur->next;
				ft_hentrydel(&to_del);
			}
			i++;
		}
		free((*ht)->table);
	}
	ft_memdel((void**)ht);
}
