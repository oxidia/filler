/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htget.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:59 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:00 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_hashkey(const char *key)
{
	int	sum;

	sum = 0;
	while (*key)
	{
		sum += *key;
		key++;
	}
	return (sum);
}

t_hentry	*ft_htget(t_hashtable *ht, const char *key)
{
	int			index;
	t_hentry	*cur;

	index = ft_hashkey(key) % ht->size;
	cur = ht->table[index];
	while (cur != NULL)
	{
		if (ft_strequ(cur->key, key))
			break ;
		cur = cur->next;
	}
	return (cur);
}
