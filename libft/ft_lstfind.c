/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:27 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:28 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstfind(t_list *lst, void *data, int (*cmp)(void*, void*))
{
	while (lst != NULL)
	{
		if ((*cmp)(data, lst->content) == 0)
			return (lst);
		lst = lst->next;
	}
	return (NULL);
}
