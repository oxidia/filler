/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 14:30:38 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 14:31:52 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
# define VECTOR_H

# include "libft.h"

typedef struct	s_vector
{
	int	y;
	int	x;
}				t_vector;

t_vector		*ft_vectornew(int x, int y);
void			ft_vectorset(t_vector *vector, int x, int y);
void			ft_vectordel(t_vector **vector);

#endif
