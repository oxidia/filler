/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 14:30:34 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 14:30:35 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIECE_H
# define PIECE_H

# include "libft.h"
# include "helper.h"
# include "vector.h"

typedef struct	s_piece
{
	t_vector	*coords;
	int			size;
	int			min_row;
	int			min_col;
	int			rows;
	int			cols;
}				t_piece;

t_piece			*ft_piecenew(void);
int				ft_pieceinit(t_piece *piece, const int fd);
void			ft_piecedel(t_piece **piece);

#endif
