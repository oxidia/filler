/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delarray.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:58:14 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:58:45 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "helper.h"

void	ft_delarray(char ***array, int rows)
{
	int		i;
	char	**cur;

	if (array == NULL || *array == NULL)
		return ;
	i = 0;
	cur = *array;
	while (i < rows)
	{
		ft_strdel(cur + i);
		i++;
	}
	free(*array);
	*array = NULL;
}
