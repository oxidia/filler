/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vectornew.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:57:51 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:57:52 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

t_vector	*ft_vectornew(int x, int y)
{
	t_vector	*vect;

	vect = (t_vector*)ft_memalloc(sizeof(t_vector));
	if (vect == NULL)
	{
		vect->x = x;
		vect->y = y;
	}
	return (vect);
}
