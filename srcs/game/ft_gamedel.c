/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gamedel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 16:00:59 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 16:00:59 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

void	ft_gamedel(t_game **game)
{
	if (game == NULL || *game == NULL)
		return ;
	ft_boarddel(&(*game)->board);
	ft_piecedel(&(*game)->piece);
	ft_memdel((void**)game);
}
