/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pieceinit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:54:21 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/10/01 14:05:22 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

static void	ft_setmins(t_piece *piece, char **token)
{
	int	r;
	int	c;

	piece->min_row = -1;
	piece->min_col = piece->cols - 1;
	r = 0;
	while (r < piece->rows)
	{
		c = 0;
		while (c < piece->cols)
		{
			if (token[r][c] == '*')
			{
				if (piece->min_row == -1)
					piece->min_row = r;
				piece->min_col = MIN(c, piece->min_col);
				break ;
			}
			c++;
		}
		r++;
	}
}

static int	ft_setcoords(t_piece *piece, char **token, int len)
{
	int	row;
	int	col;
	int	i;

	if (!(piece->coords = (t_vector*)ft_memalloc(sizeof(t_vector) * len)))
		return (1);
	piece->size = len;
	ft_setmins(piece, token);
	row = -1;
	i = 0;
	while (++row < piece->rows)
	{
		col = -1;
		while (++col < piece->cols)
		{
			if (token[row][col] == '*')
			{
				ft_vectorset(piece->coords + i, col - piece->min_col,
					row - piece->min_row);
				i++;
			}
		}
	}
	return (0);
}

static int	ft_gettoken(t_piece *piece, const int fd)
{
	char	**token;
	char	*line;
	int		error;
	int		i;
	int		coords_len;

	if (!(token = (char**)ft_memalloc(sizeof(char*) * piece->rows)))
		return (1);
	i = -1;
	coords_len = 0;
	while (++i < piece->rows)
	{
		if ((error = ft_getline(fd, &line) != 1) == 0)
		{
			*(token + i) = line;
			error = (int)ft_strlen(line) != piece->cols;
			coords_len += ft_countof(line, '*');
		}
		if (error == 1)
			break ;
	}
	if (error == 0)
		error = ft_setcoords(piece, token, coords_len);
	ft_delarray(&token, piece->rows);
	return (error);
}

int			ft_pieceinit(t_piece *piece, const int fd)
{
	char	*line;
	int		error;

	error = ft_getline(fd, &line) != 1;
	if (error == 0)
	{
		error = !ft_strstartswith(line, "Piece ");
		if (error == 0)
		{
			error = ft_isnumber(line + 6, ' ') == 0;
			if (error == 0)
				piece->rows = ft_atoi(line + 6);
			if (error == 0)
				error = ft_isnumber(line + 6 +
					ft_nbrlen(piece->rows) + 1, ':') == 0;
			if (error == 0)
				piece->cols = ft_atoi(line + 6 + ft_nbrlen(piece->rows));
			if (error == 0)
				error = ft_gettoken(piece, fd);
		}
		ft_strdel(&line);
	}
	return (error);
}
