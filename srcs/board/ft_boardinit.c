/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boardinit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:26:15 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:27:04 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

static void	ft_lineinit(char *dest, char *src)
{
	size_t	i;

	i = 0;
	while (*(src + i))
	{
		*(dest + i) = (char)ft_tolower(*(src + i));
		if (*(dest + i) == 'o')
			*(dest + i) = -1;
		else if (*(dest + i) == 'x')
			*(dest + i) = -2;
		i++;
	}
	*(dest + i) = '\0';
}

static void	ft_fillborders(t_board *board, int row, int col, int val)
{
	int	y;
	int	x;
	int	i;
	int	j;

	i = -1;
	while (i < 2)
	{
		y = row + i;
		j = -1;
		if (y >= 0 && y < board->rows)
		{
			while (j < 2)
			{
				x = col + j;
				if (x < 0 || x >= board->cols)
					break ;
				if (board->board[y][x] == '.' &&
					board->board[y][x] != 'x' && board->board[y][x] != 'o')
					board->board[y][x] = val;
				j++;
			}
		}
		i++;
	}
}

static void	ft_fill(t_board *brd, char enmey, char score)
{
	int		y;
	int		x;
	int		call;

	y = 0;
	call = 0;
	while (y < brd->rows)
	{
		x = 0;
		while (x < brd->cols)
		{
			if (brd->board[y][x] == enmey)
			{
				ft_fillborders(brd, y, x, score);
				call = 1;
			}
			x++;
		}
		y++;
	}
	enmey = score;
	if (call)
		ft_fill(brd, enmey, score + 1);
}

int			ft_boardinit(t_board *board, int enemy, const int fd)
{
	int		i;
	int		error;
	char	*line;
	char	*buff;

	if ((error = ft_getline(fd, &line) != 1) == 1)
		return (1);
	ft_strdel(&line);
	i = -1;
	while (++i < board->rows)
	{
		if ((error = ft_getline(fd, &line) != 1) == 1)
			break ;
		buff = ft_strchr(line, ' ');
		error = !buff || (int)ft_strlen(buff + 1) != board->cols;
		if (error == 0)
			ft_lineinit(line, buff + 1);
		ft_strdel(board->board + i);
		board->board[i] = line;
		if (error == 1)
			break ;
	}
	if (error == 0)
		ft_fill(board, enemy, 1);
	return (error);
}
