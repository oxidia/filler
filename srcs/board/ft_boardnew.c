/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boardnew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:26:27 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:26:28 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

t_board	*ft_boardnew(int rows, int cols)
{
	t_board	*board;

	board = (t_board*)ft_memalloc(sizeof(t_board));
	if (board != NULL)
	{
		board->rows = rows;
		board->cols = cols;
		board->board = (char**)ft_memalloc(sizeof(char*) * rows);
		if (board == NULL)
			ft_memdel((void**)&board);
	}
	return (board);
}
